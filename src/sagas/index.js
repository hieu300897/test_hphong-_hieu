import { all } from 'redux-saga/effects'
import { ItemSaga } from './ItemSaga'
import { addPlayerSaga } from './AddSaga'
import { deletePlayerSaga } from './deleteSaga'
import {updatePlayerSaga} from './updateSaga'
function* rootSaga() {
    yield all([
        ...ItemSaga,
        ...addPlayerSaga,
        ...deletePlayerSaga,
        ...updatePlayerSaga 
    ]);
}
export default rootSaga;