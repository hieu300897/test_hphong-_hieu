import { put, takeEvery } from 'redux-saga/effects'
import savePlayerAPI from '../fetchAPI/AddAPI'
import * as types from '../constant';

function* addPlayer(action) {
    try {

        const response = yield savePlayerAPI(action.payload);
        yield put({
            type: types.ADD_ITEM_SUCCESS,
            payload: response
        })
        yield put({
            type: types.GET_ITEM_REQUEST,
        })
    }

    catch (error) {
        yield put({
            type: types.ADD_ITEM_FAILURE,
            payload: {
                errorMessage: error.message
            }
        })
    }
}
export const addPlayerSaga = [
    takeEvery(types.ADD_ITEM_REQUEST, addPlayer)
];