import { put,takeEvery } from 'redux-saga/effects'
import * as types from '../constant'
import updateFetchAPI from '../fetchAPI/updateAPI'

function* updatePlayer(action) {
    try{
       const res = yield updateFetchAPI(action.payload)
        yield put ({
            type:types.UPDATE_ITEM_SUCCESS,
            payload:res
        })
        yield put ({
            type:types.GET_ITEM_REQUEST,

        })
        
    }
    catch (error) {
        yield put({
            type: types.UPDATE_ITEM_FAILURE,
            payload: {
                errorMessage: error.message
            }
        })

    }
}
export const updatePlayerSaga =[
    takeEvery(types.UPDATE_ITEM_REQUEST,updatePlayer)
]