import * as types from '../constant'
export function getListItem(payload){
    return ({
        type: types.GET_ITEM_REQUEST,
        payload
    })
}
export function Add(payload){
    return({
        type:types.ADD_ITEM_REQUEST,
        payload
    })
}
export function deletePlayer(payload){
    return({
        type:types.DELETE_ITEM_REQUEST,
        payload
    })
}
export function update(payload){
    return({
        type:types.UPDATE_ITEM_REQUEST,
        payload
    })
}