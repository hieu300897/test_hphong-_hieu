import React, { Component } from 'react'


class Items extends Component {
    constructor(props){
        super(props);
        this.state = {
            hoten:'',
            ngaysinh:'',
            quequan:'',
            id:''

        }
    }
    onClick(item){
        console.log("gia tri di len,.....",item);
        
        this.setState({
            hoten:item.hoten,
            ngaysinh:item.ngaysinh,
            quequan:item.quequan,
            id:item.id
        })
    }
    deletePlayers(id){
        console.log("cccccccccc......",id)
        this.props.deletePlayerItem(id)
    }
    Add(){
        console.log(this.state.hoten);
        
        this.props.Add({
            hoten:this.state.hoten,
            ngaysinh:this.state.ngaysinh,
            quequan:this.state.quequan
        })

    }
   update(){
    this.props.updatePlayer({ 
        hoten:this.state.hoten,
        ngaysinh:this.state.ngaysinh,
        quequan:this.state.quequan,
        id:this.state.id
        
    })

   }
    render() {
        let listData = []
        if (this.props.items) { listData = this.props.items.map((item,key) =>
            {
                return(
                    <tr key={key} onClick={()=> this.onClick(item)}>
                        <th>{item.id}</th>
                        <th>{item.name}</th>
                        <th>{item.hoten}</th>
                        <th>{item.ngaysinh}</th>
                        <th>{item.quequan}</th>
                        <th><button onClick={()=>this.deletePlayers(item.id)}>xoa</button></th>
                        </tr>
                        
                )
            })
        }
        return (
            <div >
                <div>
                    <table className="list-item">
                        <tbody>
                            <tr>
                            <td className="Hoan">Họ Tên</td>
                            <td className="Hoan">Ngày sinh </td>
                            <td className="Hoan">Quê Quán</td>
                            
                            </tr>
                            <tr>
                            <td className="h"><input value={this.state.hoten} onChange={(e) => { this.setState({ hoten: e.target.value }) }}></input></td>
                            <td className="h"><input value={this.state.ngaysinh} onChange={(e) => { this.setState({ ngaysinh: e.target.value }) }}></input></td>
                            <td className="h"><input value={this.state.quequan} onChange={(e) => { this.setState({ quequan: e.target.value }) }}></input></td>
                            <td ><button type="button" onClick={() => this.Add()}>Thêm</button></td>
                            <td ><button type="button" onClick={() => this.update()}>Sua</button></td>
                            </tr>
                            {listData}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default Items;