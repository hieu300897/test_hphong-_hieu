import React from 'react'
import Items from '../components/Items'
import * as actions from '../actions/ItemPageActions'
import { connect } from 'react-redux'
class ItemPageContainer extends React.Component {
    componentDidMount() {
        this.props.initLoad()
        // this.props.getItem()
    }

    render() {
        return (
            <Items {...this.props} />
        );
    }
}
const mapStateToProps = (state) => {
    return {
        items: state.items.listItem


    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        initLoad: (DATA) => {
            dispatch(actions.getListItem(DATA))
        },
        Add: (data) => {
            dispatch(actions.Add(data))
        },
        deletePlayerItem: (data) => {
            dispatch(actions.deletePlayer(data))
        },
        updatePlayer:(data) =>{
            dispatch(actions.update(data))
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(ItemPageContainer)